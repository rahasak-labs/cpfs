# ipfs-cluster-ctl

### about ipfs-cluster-ctl

`ipfs-cluster-ctl` is the IPFS cluster management tool. It can be used as the client application to manage the cluster nodes and perform actions. `ipfs-cluster-ctl` uses the HTTP API provided by the nodes and it is completely separate from the cluster service.

For more information, please check the [Documentation](https://cluster.ipfs.io/documentation), in particular the [`ipfs-cluster-ctl` section](https://cluster.ipfs.io/documentation/ipfs-cluster-ctl).

### usage ipfs-cluster-ctl

Usage information can be obtained by running

```
ipfs-cluster-ctl --help
```

Usage of specific command can be obtained by running

```
ipfs-cluster-ctl help [cmd]
```

The (`--host`) can be used to talk to any remote cluster peer (`localhost` is used by default).
